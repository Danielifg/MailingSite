USE MAILINGDB;

DROP TABLE IF EXISTS CLIENTS;

CREATE TABLE CLIENTS (
CLIENTID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  LASTNAME varchar(30) NOT NULL DEFAULT '',
  FIRSTNAME varchar(30) NOT NULL DEFAULT '',
  PASSWORD varchar(16) NOT NULL DEFAULT '',
  EMAILADRESS varchar(30) NOT NULL DEFAULT '',
  STREETADRESS varchar(40) NOT NULL DEFAULT '',
  CITY varchar(16) NOT NULL DEFAULT '',
  PROVINCESTATE varchar(16) NOT NULL DEFAULT '',
  COUNRTY varchar(16) NOT NULL DEFAULT '',
  POSTALCODE varchar(16) NOT NULL DEFAULT ''
)ENGINE=InnoDB;

INSERT INTO CLIENTS
(LASTNAME, FIRSTNAME, PASSWORD, EMAILADRESS, STREETADRESS,CITY , PROVINCESTATE, COUNRTY, POSTALCODE) values
("Green","Kay","000001","kaygreen@concordia.ca" ,"01885", "Ohio","Anniversary Pass","United States","45233"),
("Linux","Kali","0000002","edwardsnowden@cia.org","02888","Mayfield","Monticello","South Korea","55590"),
("Kent","Clark","0000003","superman@justice.com","0355",  "Paris","Paris","France","13505"),
("Stark","Tony","551237","aironman@capcon.com","0400",    "Verona","Verona","Italy","34479"),
("Banner","Bruce","5551238","hulk@greenman.com","05888",  "Prince","Dayton","Unitet States","45454");