/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danielifguerero.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author d_flore
 */
@Entity
@Table(name = "clients", catalog = "MAILINGDB", schema = "")
@NamedQueries({
    @NamedQuery(name = "Clients.findAll", query = "SELECT c FROM Clients c")
    , @NamedQuery(name = "Clients.findByClientid", query = "SELECT c FROM Clients c WHERE c.clientid = :clientid")
    , @NamedQuery(name = "Clients.findByLastname", query = "SELECT c FROM Clients c WHERE c.lastname = :lastname")
    , @NamedQuery(name = "Clients.findByFirstname", query = "SELECT c FROM Clients c WHERE c.firstname = :firstname")
    , @NamedQuery(name = "Clients.findByPassword", query = "SELECT c FROM Clients c WHERE c.password = :password")
    , @NamedQuery(name = "Clients.findByEmailadress", query = "SELECT c FROM Clients c WHERE c.emailadress = :emailadress")
    , @NamedQuery(name = "Clients.findByStreetadress", query = "SELECT c FROM Clients c WHERE c.streetadress = :streetadress")
    , @NamedQuery(name = "Clients.findByCity", query = "SELECT c FROM Clients c WHERE c.city = :city")
    , @NamedQuery(name = "Clients.findByProvincestate", query = "SELECT c FROM Clients c WHERE c.provincestate = :provincestate")
    , @NamedQuery(name = "Clients.findByCounrty", query = "SELECT c FROM Clients c WHERE c.counrty = :counrty")
    , @NamedQuery(name = "Clients.findByPostalcode", query = "SELECT c FROM Clients c WHERE c.postalcode = :postalcode")})
public class Clients implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CLIENTID")
    private Integer clientid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "LASTNAME")
    private String lastname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "FIRSTNAME")
    private String firstname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "PASSWORD")
    private String password;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "EMAILADRESS")
    private String emailadress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "STREETADRESS")
    private String streetadress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "CITY")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "PROVINCESTATE")
    private String provincestate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "COUNRTY")
    private String counrty;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "POSTALCODE")
    private String postalcode;

    public Clients() {
    }

    public Clients(Integer clientid) {
        this.clientid = clientid;
    }

    public Clients(Integer clientid, String lastname, String firstname, String password, String emailadress, String streetadress, String city, String provincestate, String counrty, String postalcode) {
        this.clientid = clientid;
        this.lastname = lastname;
        this.firstname = firstname;
        this.password = password;
        this.emailadress = emailadress;
        this.streetadress = streetadress;
        this.city = city;
        this.provincestate = provincestate;
        this.counrty = counrty;
        this.postalcode = postalcode;
    }

    public Integer getClientid() {
        return clientid;
    }

    public void setClientid(Integer clientid) {
        this.clientid = clientid;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmailadress() {
        return emailadress;
    }

    public void setEmailadress(String emailadress) {
        this.emailadress = emailadress;
    }

    public String getStreetadress() {
        return streetadress;
    }

    public void setStreetadress(String streetadress) {
        this.streetadress = streetadress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvincestate() {
        return provincestate;
    }

    public void setProvincestate(String provincestate) {
        this.provincestate = provincestate;
    }

    public String getCounrty() {
        return counrty;
    }

    public void setCounrty(String counrty) {
        this.counrty = counrty;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clientid != null ? clientid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Clients)) {
            return false;
        }
        Clients other = (Clients) object;
        if ((this.clientid == null && other.clientid != null) || (this.clientid != null && !this.clientid.equals(other.clientid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.danielifguerero.mailinglistbuilder.Clients[ clientid=" + clientid + " ]";
    }
    
}
