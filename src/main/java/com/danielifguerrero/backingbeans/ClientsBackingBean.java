/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danielifguerrero.backingbeans;


import com.danielifguerero.entities.Clients;
import com.danielifguerero.entities.ClientsJpaController;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.client.Client;

/**
 *
 * @author d_flore
 */
@Named("Clients")
@RequestScoped
public class ClientsBackingBean implements Serializable{
    @Inject
    private ClientsJpaController jpa;
    
    private Clients clients;
    
    private String lastName;

    public Clients getClients() {
        return clients;
    }

    public void setClients(Clients clients) {
        this.clients = clients;
    }

       public void insertNewClient() {
        jpa.create(clients.setLastname("sdd"));
       
    }
}
